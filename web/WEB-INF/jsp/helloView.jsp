<%-- 
    Document   : helloView
    Created on : 2013/06/23, 15:58:46
    Author     : nakajimayuji
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hello</title>
    </head>
    <body>
        <h1>${helloMessage}</h1>
    </body>
</html>
